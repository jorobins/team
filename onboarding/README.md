# Tangible AI Onboarding Information

Welcome to Tangible AI team! We're excited to have you join us. 

This document summarizes the topics you're expected to be familiar with at the beginning of your time with us. 


## Python 
Check out [Hobson's blog post](https://tangibleai.com/getting-started-with-python-and-data-analysis) for resources on Python

## Open Source 
* The values of Open Source ([ Read "The open source way" on opensource.com ](https://opensource.com/open-source-way))
* Open Source Software Licenses (Check out [this guide](https://medium.com/nybles/software-licenses-c22a71f8377f) or visit [Choose a license](https://choosealicense.com/) to dig deeper)

## Web Development Basic Concepts 

* RESTful APIs ([Beginner's guide to RESTful APIs](https://mlsdev.com/blog/81-a-beginner-s-tutorial-for-understanding-restful-api))
* JSON ([Beginners' Tutorial to JSON](https://beginnersbook.com/2015/04/json-tutorial/))
