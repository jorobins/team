# GitLab Resources & Content

## Table of Contents
1. [What is GitLab?](#what-is-gitlab?)
    a. [TangibleAI.git](#tangibleai.git)
    b. [GitLab vs GitHub](#gitlab-vs-github)
2. [Install](#how-to-install)
    a. Windows (#windows)
    b. Linux (#linux)
    c. Mac (#mac)
3. [Uses and Application](#uses-and-application)
    a. [Connecting your PC to git via Terminal](#connecting-your-pc-to-git-via-terminal) 
4. [Advanced Techniques](advanced-techniques)
    a. [Creating, Managing, and Working with a Directory](creating,-managing,-and-working-with-a-directory)

### What is Gitlab?

    GitLab is software developer platform that allows for multiple users to contribute to a project with a streamlined software workflow. 

#### TangibleAI.git
    [TangibleAI git repository](https://gitlab.com/tangibleai)

#### GitLab vs GitHub
    Tangible AI has decided to go with GitLab over GitHub. Both services provide a similiar experience. The primary difference between the two is that GitLab is primarily open sourced by nature. GitHub is a subsidiary of Microsoft. 

### Install

#### Windows
    1. Open up the [Anaconda Navigator](@conda) 
    2. Then click on the Powershell Prompt
        [Powershell Prompt](@installw01)
    3. Type in the following
        ```
        git --version
        ```
    4. If you do not have git installed (likely) you should get the following pop up. 
        [git --version](@installw02)
    5. You can download and install git [here](https://git-scm.com/downloads) download link
        a. Before you install this program, [Sublime Text](@sublime-text), think of it as a Microsoft Word FOR CODE. It can be used with Python (just like Pycharm alongside many others). In the future we may need it with git. [Link is here](https://www.sublimetext.com/3)
        b. When installing git you will see the following window change it from VIM --> Sublime Text, refer to the image if neccessary. 
        [git sublime text](@installw03)
        c. Then hit next until finished. 
        d. Reboot your computer (You have to do it, I tried not too and it wouldnt work)
    6. Re do step 3. 

Congratulations git has been installed!

#### Linux
#### Mac

### Uses and Application
#### Connecting your PC to git via Terminal 
    1. For this next step you will need a Gitlab account and know your username. In order to find it go to the top right corner of the website after logging in your your username will be the information pass the @. Refer to the picture for more information. 
    [git account](@installw101)
    2. With the user name in hand we are now going to open up our power shell from the Anaconda Navigator again! 
    3. Put in the following command into the powershell and use your username
        ```
        git config --global user.name "your_username"
        ```
    4. Now put in your email with the following command
        ```
        git config --global user.email "your_email_address@example.com"
        ```
    5. Now let us check to see if the information is correct
        ```
        git config --global --list
        ```
    6. This is what the commands should look like in your powershell! Refer to the image
    [git commands](@installw102)
    7. Congratulations we have now connected your username and email for git with your desktop PC. Next we have to create a unique password and link the two (git and your desktop) so that both can interact correctly! 

#### Creating an SSH key 
    1. In your powershell type in the following command to double check if you have the ability to create SSH key. 
        ```
        ssh -V
        ```
    2. You should see a response similar to the following 
        ```
        OpenSSH_for_Windows_7.7p1, LibreSSL 2.6.5
        ```
    3. Now we are going to create our special password (ssh key)
        ```
        ssh-keygen -t ed25519 -C "<git lab ssh>"
        ```
    4. Press enter three times. This will save the ssh to a location and not lock it behind a further password. 
    5. So far this is what we have! Refer to image 
        [ssh password](@installw301)
    6. Now we need to copy our SSH Key in order to do that put in the following command
        ```
        cat ~/.ssh/id_ed25519.pub | clip
        ```
        a. FOR LINUX
        b. Open a second terminal and input the following command this is a small program that will allow us to copy and paste from terminal. 
            ```
            sudo apt-get install xclip
            ```
        c. Instead of the above command marked #6 put in the following
            ```
            cat ~/.ssh/id_ed25519.pub | xclip
            ```
    7. Your password is now copied! DO NOT COPY AND PASTE anything! Lets go straight to GitLab! 
    8. Click on the top right corner and go to your gitlab settings. Then we will look on the left side of the screen and click on SSH Keys. Refer to the image.
        [ssh password location](@installw302)
    9. Paste your key into the blank space labeled “key” do not put an expiration date, and then for the title use but whatever you call your pc! Now hit add key. 
        [adding an ssh key](@installw303)
    10. Now let us perform a “handshake” and have your pc meet gitlab using the password. Put in the following code
        ```
        ssh -T git@gitlab.com
        ```
    11. It will ask you if you are sure? 
        a. Put yes. 
    12. You will see a congratulations message!


### Advanced Techniques

#### Creating, Managing, and Working with a Directory

[Video Thumbnail](@vid01)
[Video Link](https://tan.sfo2.digitaloceanspaces.com/videos/howto/howto-bash-git-gitlab-nano-by-hobs-eda-2020-10-05.mp4)
