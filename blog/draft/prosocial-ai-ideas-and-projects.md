# Prosocial Machine Learning

## Previous Intern Project Ideas

- **inspiring quote**: qary skill to recommend an inspiring quote, Winter 2021, by Billy
    - 1: random quote suggester in `qary` skill
    - 2: content-based filter for unsupervised recommender
    - 3: collaborative filter for unupservised recommender
    - 4: user feedback mechanism: "Thank you!", "Perfect!", ":-)", ":(", "+1"
    - 5: reinforcement learning or "learning to rank" to improve recommendations
- **good listener**: qary skill to record user feedback and provide encouragement
    - 1: classifier to indicate whether feedback is on most recent chat
    - 2: regressor to score the quality/helpfulness/sentiment of feedback +1/+100/:-)
    - 3: followup with "thank you." or "noted" or "I'll try to do better next time"
    - 4: follow up with "would you like me to try again?" retriggering the bot after sending it the feedback
- **rap recommender**: chatbot to recommend a line of text that rhymes with the provide phrase, and maybe matches its rhythm Winter 2021, Uzi
    - 1: echo same sentence with last word replaced with rhyme
    - 2: echo same sentence with some synonym substitution and rhyme
    - 3: echo same sentence with slang substitution and rhyme
    - 4: generate multiple sentences with GPT-2 but filter those without rhyme
    - 5: and ness words to expand vocabulary of phone tokenizer and rhymer with soundex
    - 6: hard code additional destemmers for rhymer and phonifier
    - 7: phone expander/tokenizer for proper nouns or any sequence of characters
    - 8: deep learning RNN to generate phones for english words, including misspellings and slang and proper nouns (CMU phonifier/rhymer as training set)
    - 9: augment training set with proper noun lists and google n-gram viewer database 
- **nmt**: machine translation for chapter 10 in _NLPIA 2nd Ed_, English -> French/Spanish/Amharic/Chinese, Winter 2021, by Winnie and Hanna
    - 1: improve spanish translation
    - 2: french translation
    - 3: bidirectional translation
    - 4: improved translation using more GRU layers
    - 5: improved translation using transformer + GRU decoder
    - 6: amharic dataset cleaning
    - 7: chinese tokenzer + translator?
    - 8: sections/chapter in nlpia
- **tutor bot**: math tutor bot by Billy, but having trouble finding data
- **grade level bot**: grade essays 0-18 based on their grade level, by Josh
- **permit lottery prediction**: predict national park permit lottery wins by date, Winter 2021, by Winston
- **garbage classifier**: computer vision image classifier, object detector or image segmenter to identify categories of recyclable material (paper, glass,...), Winter 2021, by Billy, has a working classifier webapp 
- **stateful quiz**: adaptive quiz that choses the best question based on previous answers, Winter 2021, by Jose
- **question classifier**: use squad datset to predict questions vs. non-questions, 90% accuracy on simple 2-gram 1000-word count vectorizer, Winter 2021, by Devi
- **qa**: use BERT to answer questions based on a context paragraph, Summer 2020 by Olesya and Travis
- **fish classifier**: Identify 1 of 4 species of trout with computer vision object detector on raspberry pi, Winter 2020
- **legalese -> plain english**: translate legal terminology and complex statements to simpler language with a lower reading grade level, e.g. "subpoena" -> "document request" 
- **slang -> formal english**: also called style transfer
- **summarize long tech documents**: Fall 2020 by Gary
- **sentence paraphraser**: Reword sentences so they say the same thing but in a different way (synonyms, reordering word). N-gram viewer database from Gutenberg would be good source of 5-grams to use.
- **yodafy**: Translate sentences from English to Yoda apostrophized order (subject-object-verb)
- **dialect/style transfer**: english -> Shakespearean, 
- **nonprofit sentiment**: Twitter ad scraper targeting nonprofits (hardcoded list of nonprofit account @handles, hashtags, keywords). simple sentiment analysis to determine favorability rating and trends and geography -- [How to scrape tweets from Twitter](https://towardsdatascience.com/how-to-scrape-tweets-from-twitter-59287e20f0f1)
- **elastic search** indexing and tuning for better open domain QA
- **named entity extraction** and identification from personal tweets (from list of small nonprofits)
- **bot detector**: trained on tweets from known bot and nonbot accounts?
- **sentiment analysis**: self-labeled data from Twitter based on hash tags
- **toxic comment detector**: winter 2021 by Uzi, Kaggle dataset
- **topic analysis**: for each nonprofit brand
- **twitter search** from within qary? e.g. user: "tweets about Red Cross?" qary: "sentiment is 75% positive with a trending topic 'donate blood for fires in California'"
- **deflame**: style transfer using GPT-3 to turn toxic comments into nontoxic, helpfule 
- **breed classifier**: manually create a classifier of dog breeds using height weight length and other measurements
    - one table (csv and excel tab) for each breed
    - practice statistics (mean, std, histogram) on folds of the data for 3 widely separated breeds
    - single variate classifier for the 4 breeds
    - manual thresholds based on midpoint between means
    - add "other" class and show how this can have implications at puppy kennels, dog breeds, etc
    - add "mix" class and show how this can have implications on dogs (euthenizing impure breeds, pure breds relegated to breeding kennels, inbreeding and genetic abnormalities, human society affected by the kinds of dogs they have as companions and their attitude towards other humans of mixed ancestry)
    - making a mistake about a rotweiler could be fatal to a child buying that breed accidentally (just need to improve accuracy? or reduce false negatives on rotweillerness)

## Advanced Project Ideas

These projects can give you a head start on difficult project with code and data to go along with the project ideas:

- [kaggle](kaggle.com)
- [papers with code](paperswithcode.com)

## Several projects where the prosoial AI projects could be mentioned

- [San Diego Tech Hub -- AI for Everyone]()
- [Prosocial Machine Learning (ProML)](https://gitlab.com/hobs/proml) for Packt Publishing
    - scheduled to start in March
    - Maria to lead
- [Data Science for Digital Health]() for UCSD
- [Data Science for Good](https://sandiegotechhub.com/data-science/) at San Diego Tech Hub
    - Hobson to copy material for UCSD DSDH course
- [NLPIA 2nd Ed](https://gitlab.com/hobs/nlpia-manuscript) for Manning.com
    - Maria - lead editor
    - Geoff - transformers and attention
    - Winnie (Ch 10-12) and anything RNN or translation
    - Sylvia (Ch 1-4) and anything Chinese
    - Hanna? Amharic? rare language translation
    - Gary - summarization


# 2021-01-14 Blog post GPT-3 and the AI Technological and economic singularity

Background: GPT-3 is 10 percent better at the "Zero-Shot" Penn Treebank word-level language model problem than BERT: https://paperswithcode.com/sota/language-modelling-on-penn-treebank-word

- GPT-3 stats
    - 48 transfomer layers
    - 175B learnable parameters (GPT-2 1.5B, BERT 0.4B)
    - pretrained on data unavailable to anyone but big tech
    - requires massive compute resources just to do inference (to **use** the model)
    - only the architecture is open source and it's not innovative (brute force, add layers)
- GPT-3 performance on benchmark tasks
    - GPT-3 wins on 3 out of 11 general language modeling tasks
    - GPT-2 also wins on 3 out of 11 tasks (and it requires 500x fewer parameters)
    - BERT

[GPT-3 (Generative Pretrained Transformer version 3)](https://en.wikipedia.org/wiki/GPT-3): a recurrent neural network or autoregressive model.

## backlog

[Django](https://www.educative.io/) at educative.io (by Mohammed)

## Ethics

- Hedonism
- Deontology
- Consequentialism
    - Utilitarianism
    - Pragmatics
    - Deweyanism

## Machine Learning



